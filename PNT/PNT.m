function catcherror = PNT
% help function will be here eventually
% this program is hosted on gitlab
% required software (dependencies):
%       -Psychtoolbox
%
%       -homebrew (OSX only)
%
%       -ffmpeg (installed using homebrew -- need binaries for WIN)
%           -brew install ffmpeg
%           -screen capture recorder (windows only)
%               *https://github.com/rdp/screen-capture-recorder-to-video-windows-free
%
%       -pidof
%           -brew install pidof
%
%       -Gstreamer 1.8 precompiled binaries (had to change permissions in finder (/Library/Frameworks)
%           -"help GStreamer" to get download link        


catcherror = 0; % default to no error code, if error, this variable will contain details, and a stack trace
%checkForUpdate(fileparts(mfilename('fullpath')));
[subj, runnum, item, doVideoRecord, doColorPNT] = getSessionInfo; if isempty(subj); return; end
if isempty(item); item = 1; end;

% ------------------------- user defined vars -----------------------------
% -------------------------------------------------------------------------
%doVideoRecord = true;
veryPreciseTimingNeeded = false;
dur = 30; % max duration of picture (per PNT instructions)
picdir = fullfile(fileparts(mfilename('fullpath')),'pics'); % picture folder
fext = '.png';
if doColorPNT
    picdir = fullfile(fileparts(mfilename('fullpath')),'pics_color'); % picture folder
    fext = '.bmp';
    background = [0 0 0];
    textColor = [1 1 1];
end
datadir = fullfile(fileparts(mfilename('fullpath')),'data'); % folder for saving data
if ~exist(datadir,'dir') % if data folder doesn't exist, make it
    mkdir(datadir);
end
if isnumeric(subj); subj = num2str(subj); end;
if isnumeric(runnum); runnum = num2str(runnum); end
T = readtable(fullfile(fileparts(mfilename('fullpath')),'pntstim.csv')); % read stimulus order file
datestring = getDateAndTime; % get date string down to the minute
datafile = fullfile(datadir,sprintf('%s_%s_%s_%s.csv',subj, runnum,mfilename, datestring)); % make data file name for saving
movfile = fullfile(datadir,sprintf('%s_%s_%s_%s.mkv',subj, runnum,mfilename, datestring)); % make video file name for recording
%instruction string for participant/clinician
instruct = sprintf(['I''m going to ask you to name some pictures. When you hear a beep, a picture will\n',...
                    'appear on the computer screen. Your job is to name the picture using only one\n',...
                    'word. We''ll practice several pictures before we begin.\n\n',...
                    '[Press the spacebar to begin]']);

% ------ don't edit below this line unless you know what you're doing -----
% -------------------------------------------------------------------------
spoolUpTime = 0;
setPathForFFMPEG;
KbName('UnifyKeyNames'); % make all keyboards similar
oldLevel = Screen('Preference', 'VisualDebugLevel', 1);
if veryPreciseTimingNeeded
    Screen('Preference', 'SkipSyncTests', 0); %#ok
else
    Screen('Preference', 'SkipSyncTests',2);
end
PsychDefaultSetupPlus(3); % set up PTB with some standard values
if doColorPNT
    params = PsychSetupParams(0,1, background, textColor); % set some standard parameters for this session
else
    params = PsychSetupParams(0,1); % set some standard parameters for this session
end

n = size(T,1); % number of trials is the number of rows in Table T
%start ffmpeg recording
if doVideoRecord
    spoolUpTime = startRecording(movfile, 29.97); % start recording video with a 29.97fps framerate (a common value)
end
commandwindow;
startTime = GetSecs-spoolUpTime;
ShowInstructions(params, instruct, {'space', 'escape'});
WaitSecs(0.5); % wait for 500 ms just to have a smooth transition from instruct to task
try
    i = item;
    keys = {'escape', 'RightArrow', 'c', 's', 'm', 'u', 'n', 'f', 'p', 'd', 'o', 'x'};
    %key legend:
    % c = correct
    % s = semantic error
    % m = mixed/phonological and semantic
    % u = unrelated
    % n = nonwords related phonologically
    % f = real words related phonologically
    % p = perseveration
    % d = description
    % o = no response
    % x = miscellaneous
    % RightArrow = skip, defaults to no response
    % escape = quit the program
    while i <= n
        if doColorPNT
            imgname = fullfile(picdir,[deblank(char(T.PictureName(i))) '_' sprintf('%03d',T.OrderNum(i)) fext]); % construct image name
        else
            imgname = fullfile(picdir,[deblank(char(T.PictureName(i))) fext]); % construct image name
        end
        picTime = GetSecs;
        responseKey = showStimulus(params, imgname, dur,keys,T.practice(i,1)); % do a trial
        T.response{i,1} = responseKey; 
        if doVideoRecord
            T.startTimePointInVideo{i,1} = picTime - startTime;
            T.endTimePointInVideo{i,1} = GetSecs - startTime;
        end
        writetable(T,datafile);
        dkey = showInterTrialScreen(params, {'LeftArrow', 'RightArrow','escape'});
        switch dkey
            case 'LeftArrow'
                % do nothing
            case 'RightArrow'
                i = i+1;
        end
    end
    Screen('Preference', 'VisualDebugLevel', oldLevel);
    CleanUp({datafile, movfile});
    
catch catcherror
    CleanUp({datafile, movfile});
end
end
%sub functions below





















function datestring = getDateAndTime
d = fix(clock);
datestring=sprintf('Y%04d_M%02d_D%02d_H%02d_M%02d_S%02d',...
    d(1),...
    d(2),...
    d(3),...
    d(4),...
    d(5),...
    d(6));
end


function startTime = ShowInstructions(params, instruct,keysToWaitFor)
if nargin < 3; keysToWaitFor = {'space', 'escape'}; end;
Screen('Flip',params.win); % for windows
DrawFormattedText(params.win, instruct, 'center', 'center',params.TextColor);
Screen('Flip',params.win);
RestrictKeysForKbCheck(cellfun(@KbName, keysToWaitFor));
deviceN = -1;
[startTime, keyCode] = KbWait(deviceN);
if strcmpi(KbName(keyCode), 'escape')
    CleanUp;
end
RestrictKeysForKbCheck([]);
Screen('Flip',params.win);
end


function CleanUp(files)
if nargin < 1
    files = [];
end
ListenChar(0);
sca;
RestrictKeysForKbCheck([]);
stopRecording
putFilesInDropbox(files);
end %CleanUp


function responseKey = showStimulus(params,imgname, dur,keys,practice)
doBeepSound; % play beep to signal start of new trial
RestrictKeysForKbCheck(cellfun(@KbName, keys)); % only looks for keys we care about
timerStart = GetSecs; % start a timer to keep track of elapsed time
img = imread(imgname); % read pic into memory
imgsize = size(img); % get pic size
params.picRect = [0 0 imgsize(2) imgsize(1)]; % get rect in order to center on screen
params.picRect = CenterRectOnPoint(params.picRect,params.Xc,params.Yc); % center on screen
Screen('Flip',params.win); % clear the screen
trialElapsedTime = 0; % init elapsed time variable with zero value (will count up to dur)
keyIsPressed = 0; % init repsonse checker
responseKey = 'o'; % init response key (default to "No Response (o)" code
% while a key hasn't been pressed, and the elapsed time is less than dur
while ~keyIsPressed & trialElapsedTime < dur %#ok
    [keyIsPressed, ~, keyCode] = KbCheck(-1); % check for keyboard press
    tex = Screen('MakeTexture', params.win, img); % make texture
    Screen('DrawTexture', params.win, tex, [], [], 0); % draw the texture in back buffer
    if practice
        DrawFormattedText(params.win,'Practice','center',params.tsize+5,params.colors.black);
    end
    t = Screen('Flip', params.win);% flip image to screen 
    Screen('Close',tex);
    if keyIsPressed % check if a response happened
        responseKey = KbName(keyCode);
        if strcmpi(responseKey, 'escape') % if escape was pressed then exit the session
            CleanUp;
        end
    end
    trialElapsedTime = t - timerStart; % keep track of elapsed time
    
end
Screen('Flip', params.win); % clear the screen
KbReleaseWait(-1);
end


function PsychDefaultSetupPlus(featureLevel)
% PsychDefaultSetup(featureLevel) - Perform standard setup for Psychtoolbox.

% Default colormode to use: 0 = clamped, 0-255 range. 1 = unclamped 0-1 range.
global psych_default_colormode;
psych_default_colormode = 0;

% Reset KbName mappings:
clear KbName;

% Define maximum supported featureLevel for this Psychtoolbox installation:
maxFeatureLevel = 3;

% Sanity check featureLevel argument:
if nargin < 1 || isempty(featureLevel) || ~isscalar(featureLevel) || ~isnumeric(featureLevel) || featureLevel < 0
    error('Mandatory featureLevel argument missing or invalid (not a scalar number or negative).');
end

% Always AssertOpenGL:
AssertOpenGL;

% Level 1+ requested?
if featureLevel >= 1
    % Unify keycode to keyname mapping across operating systems:
    KbName('UnifyKeyNames');
end

% Level 2+ requested?
if featureLevel >= 2
    % Initial call to timing functions
    % Set global environment variable to ask PsychImaging() to enable
    % normalized color range for all drawing commands and Screen('MakeTexture'):
    psych_default_colormode = 1;
    GetSecs; WaitSecs(0.001);
end

% Level 2+ requested?
if featureLevel >= 3
    %suppress keypress to command window,
    %and hide the mouse pointer (usefull is most visual experiments)
    ListenChar(2);
    HideCursor;
end


if featureLevel > maxFeatureLevel
    error('This installation of Psychtoolbox can not execute scripts at the requested featureLevel of %i, but only up to level %i ! UpdatePsychtoolbox!', featureLevel, maxFeatureLevel);
end
return;
end

function params = PsychSetupParams(doAlphaBlending,doMultiSample, background, textColor)
%sets up some normal values used in experiments such as a gray background
%and Arial font, and a large text size, etc...
%saves all relevant screen info to the 'params' structure so that the
%entire structure can be passed in and out of functions, rather than
%zillions of variables. Also makes it expandable.
%
% History:
% 29-May-2015   th     made initial version of the function
if nargin < 3
    background = [1 1 1];
end
if nargin < 4
    textColor = [0 0 0];
end

global psych_default_colormode;
%make params structure
params = struct;
%set some defualt, common colors
params.colors.white = [1 1 1];
params.colors.black = [0 0 0];
params.colors.gray = [0.5 0.5 0.5];
params.colors.red = [1 0 0];
params.colors.green = [0 1 0];
%check if using normalized color values or not
if psych_default_colormode == 0
    params.colors.white = [255 255 255];
    params.colors.gray = [128 128 128];
end
%choose max screen number (will be the external monitor if connected)
params.screen = max(Screen('Screens'));
params.font = 'Arial'; %set the global font for PTB to use
params.tsize = 18; %set text size
params.TextColor = textColor; %set global text color
%set the background color of the screen (defaults to gray)
params.background = background;
params.multiSample = [];
if doMultiSample
    params.multiSample = 4;%set to a value greater than 0 if you want super sampling
end
%open the PTB window
[params.win, params.rect] = PsychImaging('OpenWindow', params.screen, params.background,[],[],[],[],params.multiSample);
%get screen width and height
[params.maxXpixels, params.maxYpixels] = Screen('WindowSize', params.win);
if doAlphaBlending
    %Set blend function for alpha blending
    Screen('BlendFunction', params.win, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA');
end
%find center of screen
[params.Xc,params.Yc] = RectCenter([0 0 params.maxXpixels params.maxYpixels]);
%now that the window pointer exists, set some values from earlier
Screen('TextSize', params.win, params.tsize);
Screen('TextFont',params.win, params.font);
Screen('TextSize',params.win, params.tsize);
Screen('TextStyle', params.win, 1);

%Maximum priority level
params.topPriorityLevel = MaxPriority(params.win);
Priority(params.topPriorityLevel);
%Query the frame duration
params.ifi = Screen('GetFlipInterval', params.win);
end

function doBeepSound
amp=2; 
fs=44100;  % sampling frequency
duration=0.1;
freq=500;
values=0:1/fs:duration;
a=amp*sin(2*pi*freq*values);
sound(a,fs);
end %doBeepSound


function showBreakScreen(params, keys)
RestrictKeysForKbCheck(cellfun(@KbName, keys)); % only looks for keys we care about
DrawFormattedText(params.win, 'BREAK, press right arrow to continue','center','center',params.TextColor);
Screen('Flip', params.win);
KbWait(-1);
Screen('Flip', params.win);
WaitSecs(0.500); % for smooth transition back to task
end

function directionKey = showInterTrialScreen(params, keys)
RestrictKeysForKbCheck(cellfun(@KbName, keys)); % only looks for keys we care about
DrawFormattedText(params.win, sprintf('Left Arrow to repeat, Right Arrow for next item'),'center','center',params.TextColor);
Screen('Flip', params.win);
[~, keyCode] = KbWait(-1);
directionKey = KbName(keyCode);
Screen('Flip', params.win);
WaitSecs(0.500); % for smooth transition back to task
KbReleaseWait(-1);
end

function spoolUpTime = startRecording(movfile, frate)
%http://www.oodlestechnologies.com/blogs/PICTURE-IN-PICTURE-effect-using-FFMPEG
if nargin < 2
    frate = 30; % default value that works on my development computer (macbook 2015ish, osx 10.10.4)
end
spoolUpTime = 2;
if ismac % tested on macbook pro 2015+ i7 osx 10.10.4
    %in the future it would be nice to have a distributable binary, for now
    %we use homebrew
    %ffmpegpath = fullfile(fileparts(mfilename('fullpath')),'ffmpeg');
    ffmpegpath = 'ffmpeg';
    fmt = 'avfoundation';
    videoSize = '1280x720';
    screenInputDevice = '"1"';
    videoInputDevice = '"0"';
    audioInputDevice = '"0"';
    videoQuality = 30; % range from 0 to 60ish (lower numbers mean HUGE files, but better quality -- lossless) -- 40 seems ok 
elseif IsWin % not tested yet
    warning('FFmpeg video recording is experimental on windows'); 
    ffmpegpath = 'ffmpeg';
    fmt = 'dshow';
    videoSize = '320x240'; %smaller because.... windows....
    %------- only tested on HP ProBook Win 7 ---------%
    screenInputDevice = 'video="screen-capture-recorder"';
    videoInputDevice = 'video="HP HD Camera"';
    audioInputDevice = 'audio="Internal Microphone (Conexant S"';
    %-------------------------------------------------%
    videoQuality = 40; % range from 0 to 60ish (lower numbers mean HUGE files, but better quality -- lossless) -- 40 seems ok 
end
cmd = sprintf(['%s -y -thread_queue_size 50 ','-f %s ',...
    '-framerate %d -i %s -thread_queue_size 50 ',...
    '-f %s -framerate %d -video_size %s ',...
    '-i %s:%s -c:v libx264 -crf %d -preset ultrafast ',...
    '-filter_complex ' '"[0]scale=iw/8:ih/8 [pip]; [1][pip] overlay=main_w-overlay_w-10:main_h-overlay_h-10" ',...
    '-r %d %s &'],...
    ffmpegpath, fmt, frate, screenInputDevice, fmt, frate, videoSize, videoInputDevice, audioInputDevice,videoQuality,frate,movfile);
disp(cmd);
system(cmd);
WaitSecs('YieldSecs',spoolUpTime); % need to wait before checking if recording started -- give it time to actually start
if ~isRecording % if the recording did not start (usually due to an incompatible framerate for the hardware)
    if frate < 30
        frate = 30; % if lower framerate was tried (29.97) then try 30 now
    else
        frate = 29.97;
    end
    cmd = sprintf(['%s -y -thread_queue_size 50 ','-f %s ',...
        '-framerate %d -i %s -thread_queue_size 50 ',...
        '-f %s -framerate %d -video_size %s ',...
        '-i %s:%s -c:v libx264 -crf %d -preset ultrafast ',...
        '-filter_complex ' '"[0]scale=iw/8:ih/8 [pip]; [1][pip] overlay=main_w-overlay_w-10:main_h-overlay_h-10" ',...
        '-r %d %s &'],...
        ffmpegpath, fmt, frate, screenInputDevice, fmt, frate, videoSize, videoInputDevice, audioInputDevice,videoQuality,frate,movfile);
    disp(cmd); % display the command to matlab command window for debugging
    system(cmd);
    WaitSecs('YieldSecs',spoolUpTime); % give the hardware time to actually get going
    if ~isRecording % if still not recording, throw an error
        CleanUp;
        error('ffmpeg recording was requested, but failed to start. This may be due to an incompatible setting')
    end
end
end %startRecording

function stopRecording
if ismac
    system('killall ffmpeg');
elseif IsWin
    system('Taskkill /IM ffmpeg.exe'); % request that ffmpeg stop
    system('Taskkill /IM cmd.exe /t'); % kill command window that pops up (and all child processes)
    pause(2);
    system('Taskkill /IM cmd.exe /t'); % do it again for good measure
end
end %stopRecording

function val = isRecording
if ismac
    [~, r] = system('pidof ffmpeg');
    if isempty(r);
        val = 0;
    else
        val = 1;
    end
elseif IsWin
    [~, r] = system('tasklist /FI "IMAGENAME eq ffmpeg.exe"');
    if strfind(r, 'ffmpeg')
        val = true;
    else
        val = false;
    end
end
end % isRecording

function PATH = setPathForFFMPEG
if IsWin
    PATH = getenv('PATH');
    setenv('PATH', [PATH ';' fullfile(fileparts(mfilename('fullpath')),'winffmpeg')]);
    PATH = getenv('PATH');
    disp(PATH);
else
    PATH = getenv('PATH');
    setenv('PATH', [PATH ':/usr/local/bin']);
    PATH = getenv('PATH');
    disp(PATH);
end
end % setPathForFFMPEG

function checkForUpdate(repoPath)
prevPath = pwd;
cd(repoPath);
if exist('.git','dir') %only check for updates if program was installed with "git clone"
    [~, r] = system('git fetch origin','-echo');
    if strfind(r,'fatal')
        warning('Unabe to check for updates. Internet issue?');
        return;
    end
    [~, r] = system('git status','-echo');
    if strfind(r,'behind')
        if askToUpdate
            system('git reset --hard HEAD');
            system('git pull');
            showRestartMsg
        end
    end
else %do nothing for now
    warning('To enable updates run "!git clone git@gitlab.com:Hanayik/%s.git"',mfilename);
end
cd(prevPath);
end % checkForUpdate

function showRestartMsg
uiwait(msgbox('The program must be restarted for changes to take effect. Click "OK" to quit the program. You will need to restart it just as you normally would','Restart Program'))
exit;
end % showRestartMsg

function a = askToUpdate
% Construct a questdlg
choice = questdlg(sprintf('An update for %s is available. Would you like to update?',mfilename), ...
	'Auto update', ...
	'Yes','No','Yes');
% Handle response
switch choice
    case 'Yes'
        a = true;
    case 'No'
        a = false;
end
end % askToUpdate


function [subj, runnum, item, doVideoRecord, doColorPNT] = getSessionInfo
subj = [];
runnum = [];
doVideoRecord = [];
doColorPNT = [];
item = [];
prompt={'Participant: ','Session: ', 'Starting item number: ', 'Record video? (y,n): ', 'Use color pictures? (y,n): '};
   name='PNT settings';
   numlines=1;
   defaultanswer={'0','0','1','y','n'};
 
answer=inputdlg(prompt,name,numlines,defaultanswer);
if isempty(answer); return; end
subj = answer{1};
runnum = answer{2};
item = str2num(answer{3}); %#ok
if strcmpi(answer{4},'y'); doVideoRecord = 1; else doVideoRecord = 0; end
if strcmpi(answer{5},'y'); doColorPNT = 1; else doColorPNT = 0; end
end

function putFilesInDropbox(files)
if isempty(files); return; end;
dropboxDir = ('~/Dropbox');
if ~isdir(dropboxDir); warning('Data syncing not available. No Dropbox folder detected at %s', dropboxDir); return; end;
taskFolder = fullfile(dropboxDir,'PolarData', mfilename);
if ~isdir(taskFolder)
    mkdir(taskFolder);
end
n = size(files,2);
for i = 1:n
    [~,nm,ext] = fileparts(files{i});
    copyfile(files{i},fullfile(taskFolder,[nm ext]));
end
end










